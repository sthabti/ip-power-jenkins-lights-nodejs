var request = require('request');
var turnOnLights = require('./turnOn');
var turnOffLights = require('./turnOff');
var statusOfJob = [];

setInterval(function () {

    statusOfJob = [];

    var jenkinsJob1 = {url:'http://url', job:'jobName'};
    var jenkinsJob2 = {url:'http://url', job:'jobName'};
    var jenkinsJob3 = {url:'http://url', job:'jobName'};

    var jenkinsJobs = [jenkinsJob1, jenkinsJob2, jenkinsJob3];

    var completion = function(results){
        if (results.indexOf("failed") != -1) {
            console.log("One Of The Builds Failed");
            turnOnLights.turnOn();
        } else {
            turnOffLights.turnOff();
        }
    }

    getJenkins(jenkinsJobs, completion);

}, 1000);

function getJenkins(jobQueue, completion) {

    var jenkinsItem = jobQueue.shift();
    var jenkins = jenkinsItem.url + '/job/' + jenkinsItem.job + '/lastBuild/api/json?tree=result';

    request(jenkins, function (error, response, content) {

        if (!error && response.statusCode == 200) {

            var jsonData = JSON.parse(content);
            if (jsonData.result == "FAILURE") {
                statusOfJob.push("failed");
            } else {
                statusOfJob.push("passed");
            }
        }
        if(jobQueue.length > 0){
            getJenkins(jobQueue, completion);
        } else {
            completion(statusOfJob);
        }
    });

}



